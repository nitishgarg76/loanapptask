/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import CustomChip from './android/src/assets/constants/ChipComponent';
import PaymentCard from './android/src/assets/constants/PaymentCard';
let payment = [
  {
    date: '26 december, 2023',
    status: 'Payment successfully\nrecieved',
    amount: '₹1,400',
  },
  {
    date: '26 december, 2023',
    status: 'Payment successfully\nrecieved',
    amount: '₹1,400',
  },
  {
    date: '26 december, 2023',
    status: 'Payment successfully\nrecieved',
    amount: '₹1,400',
  },
];
const App = () => {
  return (
    <ScrollView style={{backgroundColor: '#853DBB'}}>
      <View>
        <View style={styles.box}>
          {/* <Arrow /> */}
          <View style={styles.customRow}>
            <TouchableOpacity style={styles.icon} />
            <Text style={styles.header}>CREDIT</Text>
          </View>
          <View style={styles.customRow}>
            <View style={styles.circleAvatar} />
            <TouchableOpacity style={styles.icon} />
          </View>
        </View>
      </View>
      <View style={styles.container}>
        <View style={styles.card}>
          <Text style={styles.rs}>₹ 7000</Text>
          <Text style={{fontSize: 14}}>Your Approved Limit</Text>
          <Text style={styles.customFont}>
            Utilized limit : <Text style={{color: '#0049A8'}}>₹ 20,000</Text>
          </Text>
          <Text style={{color: 'black', marginBottom: 20}}>
            Availabel : <Text style={{color: '#0049A8'}}>₹ 50,000</Text>
          </Text>
          <Text style={{fontSize: 12}}>
            {'( All dues are debited on\n5th of each month )'}
          </Text>
        </View>
        <View style={styles.chips}>
          <CustomChip label={'Bank\nTransfer'} onSelect={() => {}} />
          <CustomChip label={'Billing\nDetails'} onSelect={() => {}} />
          <CustomChip label={'My EMI\n '} onSelect={() => {}} />
          <CustomChip label={'RePay\nHistory'} onSelect={() => {}} />
        </View>
        <View style={styles.historyCard}>
          <View style={styles.row}>
            <Text style={{color: 'black', fontSize: 24, fontWeight: '500'}}>
              Transactions History
            </Text>
            <Text style={{fontSize: 12, fontWeight: '500'}}>View all</Text>
          </View>
          <Text>
            All the important transactions you want to view, track and manage in
            one place
          </Text>
          {payment?.map((item, index) => {
            return (
              <PaymentCard
                key={index}
                date={item.date}
                status={item.status}
                amount={item.amount}
              />
            );
          })}
          <TouchableOpacity style={styles.viewAll}>
            <Text style={styles.label}>View all</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bonusCard}>
          <Text style={styles.bonus}>{'Bonus reward\nunlocked'}</Text>
          <TouchableOpacity style={styles.checkButton}>
            <Text style={styles.check}>Check</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.insauranceCard}>
          <Text style={styles.cardTitle}>Loan Insurance Protection</Text>
          <Text style={{marginVertical: 20}}>
            {
              'Secure your loan repayment in case of hospitalization. Check your coverage benefits'
            }
          </Text>
          <View style={styles.insauranceBox}>
            <Text style={styles.subtitle}>
              {'Get Insurance\nProtectionFor\nYour Loan'}
            </Text>
            <Text style={{color: 'white'}}>
              {"upto 3 EMI's covered in case\nof hospitalization"}
            </Text>
            <Text style={{color: 'white'}}>
              {'loan amount covered in case\nof death or permanent\ndisability'}
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  subtitle: {color: 'white', fontSize: 25, fontWeight: '500'},
  insauranceBox: {
    backgroundColor: '#853DBB',
    padding: 20,
    borderRadius: 10,
    elevation: 10,
  },
  cardTitle: {color: 'black', fontSize: 24, fontWeight: '600'},
  insauranceCard: {
    backgroundColor: 'white',
    marginHorizontal: 20,
    marginBottom: 20,
    paddingHorizontal: 16,
    paddingVertical: 10,
    elevation: 5,
    borderRadius: 10,
  },
  check: {color: 'black', fontWeight: '500', alignSelf: 'center'},
  checkButton: {
    backgroundColor: '#D6E9CC',
    borderRadius: 17,
    width: '25%',
    paddingVertical: 10,
    marginTop: 20,
  },
  bonusCard: {
    backgroundColor: '#272727',
    margin: 20,
    padding: 20,
    borderRadius: 10,
    elevation: 10,
  },
  bonus: {color: 'white', fontSize: 30, fontWeight: '600'},
  label: {
    color: '#853DBB',
    marginVertical: 12,
    alignSelf: 'center',
  },
  viewAll: {
    borderColor: '#853DBB',
    borderWidth: 1,
    borderRadius: 18,
    width: '30%',
    alignSelf: 'center',
    marginVertical: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  historyCard: {
    backgroundColor: 'white',
    margin: 20,
    borderRadius: 12,
    paddingHorizontal: 20,
  },
  chips: {
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'space-between',
    marginTop: 20,
    alignItems: 'center',
  },
  customFont: {color: 'black', marginTop: 20},
  rs: {color: 'black', fontSize: 24, fontWeight: '500'},
  card: {
    backgroundColor: 'white',
    marginHorizontal: 20,
    paddingLeft: 20,
    paddingVertical: 20,
    // elevation: 5,
    borderRadius: 12,
    marginTop: -120,
  },
  container: {
    backgroundColor: '#F1F1F1',
    marginTop: '40%',
    flex: 1,
  },
  icon: {
    backgroundColor: 'white',
    height: 30,
    width: 40,
    marginHorizontal: 20,
  },
  circleAvatar: {
    backgroundColor: 'white',
    height: 40,
    width: 40,
    borderRadius: 50,
  },
  header: {fontSize: 31, color: 'white', fontWeight: '500'},
  customRow: {flexDirection: 'row', alignItems: 'center'},
  box: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 30,
  },
});

export default App;
