/* eslint-disable react-native/no-inline-styles */
import React, {FC} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
interface chipProps {
  label: string;
  onSelect: () => void;
}
const CustomChip: FC<chipProps> = ({...props}) => {
  return (
    <TouchableOpacity
      onPress={props.onSelect}
      style={{justifyContent: 'center', alignItems: 'center'}}>
      <View style={styles.box} />
      <Text style={styles.label}>{props.label}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  box: {
    height: 80,
    width: 80,
    borderRadius: 20,
    backgroundColor: 'white',
  },
  label: {
    color: 'black',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 5,
  },
});
export default CustomChip;
