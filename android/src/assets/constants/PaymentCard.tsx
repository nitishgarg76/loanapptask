/* eslint-disable react-native/no-inline-styles */
import React, {FC} from 'react';
import {View, Text, StyleSheet} from 'react-native';
interface cardProps {
  date: string;
  status: string;
  amount: string;
}
const PaymentCard: FC<cardProps> = ({...props}) => {
  return (
    <View style={styles.box}>
      <View style={{flex: 1}}>
        <Text style={styles.date}>{props.date}</Text>
        <Text style={styles.title}>{props.status}</Text>
      </View>
      <Text style={styles.amount}>{props.amount}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#F1F1F1',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 17,
    marginTop: 20,
    flex: 1,
  },
  date: {color: '#0068DF', fontSize: 10},
  title: {fontSize: 18, color: '#202020', fontWeight: '500'},
  amount: {color: 'black', fontSize: 17, fontWeight: '500'},
});
export default PaymentCard;
